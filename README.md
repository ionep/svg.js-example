# SVG.js-Example

#### 介绍
SVG.js  Example

本示例用于SVG.js 的学习与总结。

如果想深入学习SVG.js，请移驾 [https://svgjs.com](https://svgjs.com/)

说明： 对应的插件，请到[https://github.com/svgdotjs](https://github.com/svgdotjs)中下载

![circles](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/circles.gif)

![path_animation](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/path_animation.gif)

![polygon_filter_animation](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/polygon_filter_animation.gif)

![shapes_display](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/shapes_display.gif)

![shapes_transform](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/shapes_transform.gif)

![shapes](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/shapes.gif)

![stagger](https://gitee.com/ionep/svg.js-example/raw/master/doc/images/stagger.gif)
